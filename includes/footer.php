    <footer class="footer">
      <div class="container text-center text-light">
        <div>Made by <a class="text-warning" href="https://framagit.org/mablr/libmail/graphs/master">Team Libmail</a> under license <a class="text-warning" href="https://framagit.org/mablr/libmail/blob/master/LICENSE">GNU GPL v3</a><br><a class="text-warning" href="https://framagit.org/mablr/libmail">Voir le code source</a> - <a class="text-warning" href="cgu.php">CGU</a></div>
      </div>
    </footer>
    <script src="includes/js/jquery-3.2.1.slim.min.js"></script>
    <script src="includes/js/popper.min.js"></script>
    <script src="includes/js/bootstrap.min.js"></script>
