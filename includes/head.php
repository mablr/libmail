  <head>
    <meta charset="utf-8">
    <title>Libmail, Service Mail Libre et Respectueux de la vie Privée</title>
    <link rel="stylesheet" href="includes/css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="includes/favicon.png">
    <link href="open-iconic/font/css/open-iconic-bootstrap.css" rel="stylesheet">
    <style media="screen">
      html {
        position: relative;
        min-height: 100%;
      }
      body {
        padding-top: 6.5rem; /*for navbar*/
        padding-bottom: 10rem; /*for footer*/
      }
      .footer {
        position: absolute;
        bottom: 0;
        width: 100%;
        padding: 2rem;
        line-height: 2rem;
        background-color: #007BFF;
      }
      .padding {
        padding: 4.5%;
      }
      .brand-link {
        padding-top: 0.5rem
      }
    </style>
  </head>
  <body>
